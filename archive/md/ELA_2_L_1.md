---
title: ELA_2_L_1
subtitle: Schwingungen im Komplexen
date: 22.02.2019
tag: ELA_2_L
---
# 1 Schwingungen im Komplexen
---
## 1.1 Zeigerdarstellung
---

Seien $A>0, \omega > 0, \varphi \in \mathbb{R} \land y(t)=A\cdot sin(\omega t+\varphi)$

**Definition:** Die Funktion $\underline{y}:[t_0, t_E]\to\mathbb{C}$ mit
$\boxed{\underline{y}(t):=A\cdot e^{j(\omega\cdot t+\varphi)}}$
heisst *Komplexifizierung* von y.

Bemerkungen:
1. Die physikalische Schwingung wird durch y und *nicht* durch $\underline{y}$ beschrieben! $\underline{y}$ ist "nur" eine Hilfsfunktion.
2. Es gilt $\underline{y}(t)=A\cdot cis(\omega t+\varphi)=A\cdot\cos(\omega t+\varphi)=A\cdot\cos(\omega t+\varphi)+i\cdot y(t)\implies\underline{\underline{{y(t)=\Im(\underline{y}(t))}}}$


**Definition:** Die *Komplexe Amplitude* von y ist 
$\boxed{\underline{A}:=A\cdot e^{i\varphi}}$

Bemerkungen:
1. Es gilt: $\underline{\underline{\underline{y}(t)}}=A\cdot e^{(\omega t+\varphi)}=\underbrace{A\cdot e^{i\omega t}\cdot e^{i\cdot\varphi}}_{=\underline{A}}=\underline{\underline{\underline{A}\cdot e^{i\omega t}}}$

2. Informationsgehalt $\omega\rightarrowtail e^{i\omega t} A,\varphi \rightarrowtail\underline{A}\implies$ Jede harmonische Schwingung mit Kreisfrequenz $\omega$ kann durch eine komplexe Amplitude dargestellt werden.

![Zeigerdarstellung](../assets/zeigerdarstellung.png=300)

## 1.2 Überlagerung 
---
*Frage:* Wie kann man die Summe bzw. Differenz von zwei harmonischen Schwingungen effizient bilden?

**Satz:** Seien $\underline{A_1},\underline{A_2}\in\mathbb{C}$ die komplexen Amplituden der harmonischen Schwingungen $y_1(t)\land y_2(t).$ Dann ist die Überlagerung der beiden wieder eine harmonische Schwingung mit komplexer Amplitude

$\boxed{\underline{A}=\underline{A_1}+\underline{A_2}}$

*Beweis:* Es seien
$y_1(t)=A_1\cdot\sin(\omega t+\varphi_1)$
$y_2(t)=A_2\cdot\sin(\omega t+\varphi_2)$
$\underline{y_1}(t)=A_1\cdot e^{i\varphi_1}\cdot e^{i\omega t}=\underline{A_1}\cdot e^{i\varphi t}$
$\underline{y_2}(t)=A_2\cdot e^{i\varphi_2}\cdot e^{i\omega t}=\underline{A_2}\cdot e^{i\varphi t}$
$y(t)=y_1(t)+y_2(t)$
$\underline{y}(t)=\underline{y_1}(t)+\underline{y_2}(t)=\underline{A_1}\cdot e^{i\omega t}+\underline{A_2}\cdot e^{i\omega t}$
$=\underbrace{(A_1+A_2)}_{{=\underline{A}}\cdot e^{i\omega t}}=\underline{A}\cdot e^{i\omega t}$

Es gilt:
$\underline{\underline{\Im(\underline{y}(t))}}=\Im(\underline{A}\cdot e^{i\omega t})=\Im((\underline{A_1}+\underline A_2)e^{i\omega t})$
$=\Im((A_1\cdot e^{i\varphi_1}+A_2\cdot e^{i\omega_2})\cdot e^{i\omega t})$
$=\Im((A_1\cdot e^{i(\varphi_1+\omega t)}+A_2\cdot e^{i(\varphi_2+\omega t)}$
$=\Im(A_1\cdot\cos(\varphi_1+\omega t)+A_1\cdot i\cdot\sin(\varphi_1+\omega t))+A_1\cdot\cos(\varphi_+\omega t)+A_2\cdot i\cdot\sin(\varphi_2+\omega t)$
$A_1\cdot\sin(\omega t+\varphi_1)+A_2\cdot\sin(\omega t+\varphi_2)$
$=\underline{\underline{y_1(t)+y_2(t)}}\qquad\blacksquare$

Bemerkungen:
1. Die Überlagerung von zwei harmonischen Schwingungen kann auf die Addition bzw. Subtraktion ihrer komplexen Amplitude reduziert werden.
2. Analog: $y_1(t)-y_2(t)\to\underline A_1 - \underline{A_2}$

## 1.3 Anwendung Drehstrom
---
*Situation:*
![Drehstrom](../assets/drehstrom.png.png=300)

$\begin{vmatrix}U_1(t)=A\cdot\sin(\omega t)\\U_2(t)=A\cdot\sin(\omega t+\frac {2\pi} {3})\\U_3(t)=A\cdot\sin(\omega t+\frac {4\pi} {3})\end{vmatrix}$