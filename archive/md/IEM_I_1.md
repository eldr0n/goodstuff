---
title: IEM_I_1
subtitle: Elementare Integration
date: 22.02.2019
tag: IEM_I
---
# 1. Elementare Integration
---
## 1.1 Lineare Modifikation
---
Beispiele:

+ $\int\cos(5x-3)dx=\frac 1 5\sin(5x-3)+c$
+ $\int(7x+8)^5 dx=\frac 1 7 \cdot \frac 1 6 (7x+8)^6+c$
+ $\int 3^{5x+2}dx=\frac 1 5\cdot \frac{1}{\ln(3)}\cdot3^{5x+2}+c$

**Satz:** Regel für lineare Modifikation
Sei $F'(x)=f(x)$ dann gilt:
$\boxed{\int f(m\cdot x+q)dx=\frac 1 m\cdot F(m\cdot x+q)+c}$

*Beweis:* Zusatzserie 1, Nr. 1a

+ $\int A\cdot\sin(\omega t+\varphi)dt=\frac{1}{\omega}\cdot A(-\cos(\omega t+\varphi))+c$
