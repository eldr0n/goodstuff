---
title: Test markdown
subtitle: Each post also has a subtitle
date: 06.01.2019
tag: example
---
# h1 Titel
---
## h2 bizli klinera titel

inline $\alpha$ katex

$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$

1. soso
2. fagas
3. schnäbeli
4. habasch


+ uso
+ li

> zitat
> uf mehrerna zeila

*kursiv*
**fetti mama**

```
./somestuf.sh
cd ..
ls -a
```

red rota text huibui

mark markiarta text

![artemis](assets/artemis.png=300)

[duckduckgo](https://duckduckgo.com/)

inline *kursiv* und inline **fäääät**

inline 'code'

> hanbascj
> jajo

