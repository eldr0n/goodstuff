---
title: ELA_2_L_2
subtitle: Analytische Geometrie
date: 26.02.2019
tag: ELA_2_L
---
# 2 Analytische Geometrie
---
## 2.1 Geraden
---
### 2.1.1 Parameter-Darstellung
---
*Situation:*

![Parameter-Darstellung](../assets/para_dar.png=500)

**Definition:** Sei $\vec s_0$ der Ortsvektor eines Punktes $P_0$ auf der Geraden und $\vec v\neq 0$ ein Vektor, welcher entlang der Geraden verläuft. Dann heisst
$\boxed{\vec s(t)=\vec v \cdot t + \vec s_0}$
__Parameter-Darstellung der Geraden.__

Bemerkungen:
1. Durch Variation von $t \in \mathbb R$ kann jeder Punkt der Geraden erreicht werden.
2. Die Formel ist analog zur geradliningen gleichförmigen Bewegung in der DKM-M.
3. Durch Angabe von $\vec s_p \land \vec v$ ist die Gerade eindeutig festgellegt.
4. Für jede Gerade gibt es unendlich viele Möglichkeiten ein $\vec s_0 \land v_0$ zu wählen. $\to$ Praxis $\vec s_0, v_0$ geschickt wählen.
5. Parameter-Darstellungen können in Räumen beliebiger Dimensionen verwendet werden.
6. Komponentenschreibweise in n-Dimensionen: $\vec s(t)=\vec v \cdot t + \vec s_0$

$\begin{pmatrix}x_1(t)\\...\\ x_n(t)\end{pmatrix}=\begin{pmatrix}v_1\\...\\v_n\end{pmatrix}\cdot t+\begin{pmatrix}s_{01}\\...\\s_{0n}\end{pmatrix}$

$\implies$ Ellimination von $t$ führt auf ein LGLS mit $(n-1)$ Gleichungen für $x_1,...,x_n$

*Einfache Fälle:*
$n=2:\begin{pmatrix}x(t)\\y(t)\end{pmatrix}=\begin{pmatrix}v_x\\v_y\end{pmatrix}\cdot t+\begin{pmatrix}s_{0x}\\s_{0y}\end{pmatrix}$
$\implies a\cdot x +b\cdot y+c=0$
__Geradengleichung nach Ellimination von t.__

$n=3:\begin{pmatrix}x(t)\\y(t)\\z(t)\end{pmatrix}=\begin{pmatrix}v_x\\v_y\\v_z\end{pmatrix}\cdot t+\begin{pmatrix}s_{0x}\\s_{0y}\\s_{0z}\end{pmatrix}$
$\implies a\cdot x +b\cdot y+c\cdot z+d=0$
$e\cdot x +f\cdot y+g\cdot z+d=0$
$\to$ unpraktisch wird nie verwendet.

$\to$ Serie 2, Nr.1 bis 5.