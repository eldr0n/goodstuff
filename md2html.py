#!/usr/bin/env python3

import re

# regular expressions
regex = {
    'ita' : r"([*]{1}(?=\w))(.*)([*]{1}(?!\w))",
    'bold' : r"([*]{2}(?=\w))(.*)([*]{2}(?!\w))",
    'link' : r"\n\[(.*)\]\((.*)\)",
    'pic' : r"!\[(.*)\]\((.*)\=(\d*)\D{1}",
    'icode' : r"([`]{1}(?=\w))(.*)([`]{1}(?!\w))",
    'bcode' : r"(`{3})((\n.*)*)(`{3})",
    'mark' : r"([!]{2}(?=\w))(.*)([!]{2}(?!\w))",
    'uline' : r"([__]{2}(?=\w))(.*)([__]{2}(?!\w))",
    'line' : r"(-{3})"
}

def addTags(file):

    # vars
    md = file.read()
    md = md[4:]
    file.close()
    html = []
    
    # regex replace
    regs = re.sub(regex['bold'], '<strong>\\2</strong>', md, re.M)
    regs = re.sub(regex['ita'], '<em>\\2</em>', regs, re.RegexFlag.M)
    regs = re.sub(regex['line'], '<hr>', regs, re.M)
    regs = re.sub(regex['mark'], '<mark>\\2</mark>', regs, re.M)
    regs = re.sub(regex['uline'], '<u>\\2</u>', regs, re.M)
    regs = re.sub(regex['link'], '\n<p><a href="\\2">\\1</a></p>', regs, re.RegexFlag.M)
    regs = re.sub(regex['pic'], '<p><img src="\\2" alt="\\1" width="\\3"></p>', regs, re.RegexFlag.M)
    #regs = re.sub(regex['icode'], '<code>\\1</code>', regs, re.RegexFlag.M)
    #regs = re.sub(regex['bcode'], '<pre><code>\\2</code></pre>', regs, re.RegexFlag.M)
    
    # split lines and replace others
    lines=regs.splitlines()

    for i in lines:
        # adding headers
        if i.startswith('#'):
            k = i.count('#')
            i = '<h{}>{}</h{}>'.format(k, i[k+1:], k)

        # sorted lists
        elif i.startswith('1. '):
            n = lines.index(i)
            i = '<ol><li>'+i+'</li>'
            try:
                while lines[n+1][0].isdigit():
                    lines[n] = '<li>'+lines[n]+'</li>'
                    n += 1
            except:
                lines[n] = '<li>'+lines[n]+'</li></ol>'

        # unsorted lists
        elif i.startswith('+ '):
            n = lines.index(i)
            i = '<ul><li>'+i[2:]+'</li>'
            try:
                while lines[n+1][0] == '+':
                    lines[n] = '<li>'+lines[n][2:]+'</li>'
                    n += 1
            except:
                lines[n] = '<li>'+lines[n][2:]+'</li></ul>'    

        # # blockquotes
        # elif i.startswith('> '):
        #     n = lines.index(i)
        #     i = '<blockquote><p>'+i[2:]+'</p>'
        #     while lines[n+1].startswith('>'):
        #         lines[n] = '<p>'+lines[n][2:]+'</p>'
        #         n += 1
        #     lines[n] = '<p>'+lines[n][2:]+'</p></blockquote>'



        # everything else except new lines and other html
        elif i != '' and not i.startswith('<') or i.startswith('<str') or i.startswith('<em') or i.startswith('<mar'):
            i = '<p>'+i+'</p>'
        
        
        html.append(i+'\n')
    
    return html