---
title: ELA-2-L 2
subtitle: Analytische Geometrie
date: 14.03.2019
tag: ELA_2_L
---
# 2 Analytische Geometrie
---
## 2.1 Geraden
---
### 2.1.1 Parameter-Darstellung
---
*Situation:*

![Parameter-Darstellung](../assets/para_dar.png=400)

**Definition:** Sei $\vec s_0$ der Ortsvektor eines Punktes $P_0$ auf der Geraden und $\vec v\neq 0$ ein Vektor, welcher entlang der Geraden verläuft. Dann heisst
$\boxed{\vec s(t)=\vec v \cdot t + \vec s_0}$
__Parameter-Darstellung der Geraden.__

Bemerkungen:
1. Durch Variation von $t \in \mathbb R$ kann jeder Punkt der Geraden erreicht werden.
2. Die Formel ist analog zur geradliningen gleichförmigen Bewegung in der DKM-M.
3. Durch Angabe von $\vec s_p \land \vec v$ ist die Gerade eindeutig festgellegt.
4. Für jede Gerade gibt es unendlich viele Möglichkeiten ein $\vec s_0 \land v_0$ zu wählen. $\to$ Praxis $\vec s_0, v_0$ geschickt wählen.
5. Parameter-Darstellungen können in Räumen beliebiger Dimensionen verwendet werden.
6. Komponentenschreibweise in n-Dimensionen: $\vec s(t)=\vec v \cdot t + \vec s_0$

$\begin{pmatrix}x_1(t)\\...\\ x_n(t)\end{pmatrix}=\begin{pmatrix}v_1\\...\\v_n\end{pmatrix}\cdot t+\begin{pmatrix}s_{01}\\...\\s_{0n}\end{pmatrix}$

$\implies$ Ellimination von $t$ führt auf ein LGLS mit $(n-1)$ Gleichungen für $x_1,...,x_n$

*Einfache Fälle:*
$n=2:\begin{pmatrix}x(t)\\y(t)\end{pmatrix}=\begin{pmatrix}v_x\\v_y\end{pmatrix}\cdot t+\begin{pmatrix}s_{0x}\\s_{0y}\end{pmatrix}$
$\implies a\cdot x +b\cdot y+c=0$
__Geradengleichung nach Ellimination von t.__

$n=3:\begin{pmatrix}x(t)\\y(t)\\z(t)\end{pmatrix}=\begin{pmatrix}v_x\\v_y\\v_z\end{pmatrix}\cdot t+\begin{pmatrix}s_{0x}\\s_{0y}\\s_{0z}\end{pmatrix}$
$\implies a\cdot x +b\cdot y+c\cdot z+d=0$
$e\cdot x +f\cdot y+g\cdot z+d=0$
$\to$ unpraktisch wird nie verwendet.

$\to$ Serie 2, Nr.1 bis 5.

### 2.1.2 Gerade in einer Ebene
---
*Situation:*

![Normalenvektor](../assets/normvec.png=400)

$\vec n \equiv \vec v$ um $\pi$ gedreht $\equiv \begin{pmatrix}-v_y\\v_x\end{pmatrix}=\begin{pmatrix}-n_x\\n_y\end{pmatrix}$

$\hat{a}:= \frac{\vec n}{|\vec n|}=\frac{1}{\sqrt {v_x^2+v_y^2}}\equiv\begin{pmatrix}-v_y\\v_x\end{pmatrix}\equiv$ Einheitsnormalenvektor

**Satz:** Geradengleichung
Für alle Punkte $(x;y)$ auf der Geraden gilt: 
$\boxed{n_x\cdot x+n_y\cdot y+ c =0}$
mit $c =-<\vec s_0,\vec n>$

*Beweis:* Sei $\vec s=\begin{pmatrix}x\\y\end{pmatrix}$ ein Punkt auf der Gerade, dann gilt $0=<\vec s- \vec s_0,\vec n>=<\vec s,\vec n>-<\vec s_0,\vec n>$
$=\underline{\underline{n_x\cdot x+n_y\cdot y+c}}\qquad\qquad \blacksquare$

**Satz:** Abstand Punkt-Gerade
Es gilt: 
$\boxed{d=<\vec P-\vec s_0,\hat n>}$

*Beweis:* Für ein passendes $t\in \mathbb R$ gilt
$\vec P=\vec s(t)+\vec d\qquad |-\vec s(t)$
$\vec d=\vec P - \vec s(t)=\vec P -\vec s_0 - \vec v\cdot t\qquad|<...,\hat n>$
$<\vec d,\hat n>=<\vec P -\vec s_0-\vec v\cdot t, \hat n>$
$d\cdot<\hat n, \hat n>=<\vec P-\vec s_0,\hat n>-t\cdot\underbrace{\vec v,\hat n}_{=0}$
$d\cdot\underbrace{\hat n,\hat n}_{=1}=<\vec p-\vec s_0,\hat n>$
$\underline{\underline{d=<\vec P - \vec s_0,\hat n>}}\qquad\qquad\blacksquare$

Bemerkungen:
1. In der Geradengleichung $0=<\vec s-\vec s_0,\vec n>$ kann anstelle von $\vec n$ auch $\hat n$ oder jedes von 0 verschiedene Vielfache von $\vec n$ verwendet werden. $0=<\vec s-\vec s_0,a\cdot\vec n>$ mit $a\neq0$ heisst __Normalengleichung (NG).__
2. $0=<\vec s-\vec s_0,\pm\hat n>$ heisst __Hesse-Normalform (HNF).__
3. Durch Einsetzen eines beliebigen Punktes $\vec P$ in die HNF erhält man den vorzeichenbehafteten Abstand des Punktes von der Ebene.

Beispiele:
+ NG: $3x-4y+20=0\\P=(2;-1) \implies\vec n = \begin{pmatrix}3\\-4\end{pmatrix}\implies\underline{\hat n}=\frac{1}{\sqrt{3^2+(-4)^2}}=\frac 1 5 \begin{pmatrix}3\\-4\end{pmatrix}\\ \implies HNF: \underline{\frac 3 5 x-\frac 4 5 y+4=0}\\ \implies \underline{\underline{d}}=\frac 3 5\cdot(2)-\frac 4 5 (-1)+4=\frac 6 5+\frac 4 5+4=\frac 10 5+4=2+4=\underline{\underline{6}}$
+ $\vec s(t)=\begin{pmatrix}3\\5\end{pmatrix}+\begin{pmatrix}6\\-8\end{pmatrix}\cdot t\implies \vec n=\begin{pmatrix}8\\6\end{pmatrix}\implies\underline{\hat n}=\frac{1}{\sqrt{8^2 + 6^2}}\begin{pmatrix}8\\6\end{pmatrix}=\underline{\frac{1}{10}\begin{pmatrix}8\\6\end{pmatrix}}\\ \implies c=-<\vec s_0,\hat n>=-\langle \begin{pmatrix}3\\5\end{pmatrix},\frac{1}{10}\begin{pmatrix}8\\6\end{pmatrix}\rangle\\=-\frac{1}{10}\cdot(3\cdot 8+5\cdot 6)=-\frac{1}{10}\cdot 54=-\frac{27}{5}$
$HNF:\underline{\underline{\frac 4 5x+\frac 3 5-\frac{27}{f}=0}}$

$\to$ Serie 2, Nr.1-9

### 2.1.3 Geraden im Raum
---
*Situation:*
![Parameteriesierung](../assets/gerade_para.png=400)
Parameterisierung: $\vec s(t)=\vec s_0+\vec v\cdot t$

**Satz:** Abstand Punkt-Gerade
Es gilt:
$\boxed{d=|(\vec P-\vec s) \times \hat v|}$

*Beweis:* Es gibt ein $t \in \mathbb R,$ so dass $\vec F=\vec s(t)=\vec s_0+\vec v\cdot t$
Für die Fläche A gilt: 
$|\vec v\cdot t|\cdot d=A=|(\vec P-\vec s) \times \hat v|$
$|\vec v\cdot t|\cdot d=|\vec v|\cdot t\cdot|(\vec P-\vec s) \times \hat v|$
$\implies \underline{\underline{d=|(\vec P-\vec s) \times \hat v|}}\qquad\qquad\blacksquare$

## 2.2 Ebenen im Raum
---
### 2.2.1 Parameter-Darstellung
---
![Ebene-Raum](../assets/ebene_raum.png=400)
**Definition:** Sei $\vec s_0$ der Ortsvektor eines Punktes auf der Ebene und $\vec v,\vec w\neq 0$ zwei linear unabhängige Vektoren, welche entlang der Ebene zeigen. Dann heisst
$\boxed{\vec s(\tau;\sigma)=\vec s_0+\vec v\cdot \tau+\vec w\cdot\sigma}$
__Parameter-Darstellung__ der Ebene.

Bemerkungen:
1. Durch Variation von $\tau,\sigma\in\mathbb R$ kann jeder Punkt der Ebnene erreicht werden.
2. Durch Angabe von $\vec s_0,\vec v\land\vec w$ ist die Ebene eindeutig festgelegt.
3. Für jede Ebene gibt es unendlich viele Möglichkeiten ein $\vec s_0,\vec v\land\vec w$ zu wählen. $\to$ Praxis: $\vec s_0,\vec v\land\vec w$ geschickt wählen!
4. Komponentenschreibweise in 3D: $\\\vec s(\tau;\sigma)=\vec s_0+\vec v\cdot \tau+\vec w\cdot\sigma\\ \begin{pmatrix}x(\tau;\sigma\\y(\tau;\sigma)\\t(\tau;\sigma\end{pmatrix}=\begin{pmatrix}s_{0x}\\s_{0y}\\s_{0z}\end{pmatrix}+\begin{pmatrix}v_x\\v_y\\v_z\end{pmatrix}\cdot\tau+\begin{pmatrix}w_x\\x_y\\w_z\end{pmatrix}\cdot\sigma$$\implies$ Elimination von $\tau\land\sigma$ führt auf __eine__ Gleichung für $x,y,z$.$\\ \underline{\underline{a\cdot x+b\cdot y+c\cdot t+d=0}}$

### 2.2.2 Normale & Normalengleichung
---
Normalenvektor: 
$\vec n:= \vec v\times \vec w$
$\hat n:= \frac{1}{|\vec n|}\cdot \vec n\equiv$ Einheitsnormalenvektor.

**Satz:** Ebenengleichung
Für alle Punkte $(x;y;z)$ auf der Ebene gilt: 
$\boxed{n_x\cdot x+n_y\cdot y+n_z\cdot z+d=0}$
mit $d=-<\vec s_0, \vec n>.$

*Beweis:* Sei $\vec s = \begin{pmatrix}x\\y\\z\end{pmatrix}$ ein Punkt auf der Ebene, dann gilt:
$\underline{\underline{0}}=<\vec s-\vec s_0,\vec n>=<\vec s,\vec n>-<\vec s_0,\vec n>=\Bigg\langle\begin{pmatrix}x\\y\\z\end{pmatrix},\begin{pmatrix}n_x\\n_y\\n_z\end{pmatrix}\Bigg\rangle-<\vec s_0,\vec n>$
$=\underline{\underline{n_x\cdot x+n_y\cdot y+n_z\cdot z+d=0}}\qquad\qquad\blacksquare$

**Satz:** Abstand Punkt-Ebene
Es gilt:
$\boxed{d=<\vec P-\vec s_0,\hat n>}$
*Beweis:* Für passende $\tau,\sigma\in\mathbb R$ gilt
$\vec P=\vec s(\tau;\sigma)+\vec d\qquad\qquad|-\vec s(\tau;\sigma)$
$\vec d=\vec P-\vec s(\tau;\sigma)=\vec P-\vec s_0-\vec v-\vec w\cdot\sigma\qquad\qquad|<...,\hat n>$
$<\vec d,\hat n>=<\vec P-\vec s_0-\vec v\cdot\tau-\vec w\cdot\sigma,\hat n>$
$< d\cdot\hat n,\hat n>=<\vec P-\vec s_0,\hat n>-\tau\underbrace{<\vec v,\hat n>}_{=0}-\sigma\cdot \underbrace { <\vec w,\hat n>}_{=0}$
$d\cdot\underbrace{<\hat n,\hat n>}_{=1}=<\vec P-\vec s_0,\hat n>$
$\underline{\underline{d=<\vec P-\vec s_0,\hat n>}}\qquad\qquad\blacksquare$

Bemerkungen:
1. In der Ebenengleichung $0=<\vec s-\vec s_0,\vec n>$ kann anstelle von $\vec n$ auch $\hat n$ oder jedes von $0$ verschiedene Vielfach von $\vec n$ verwendet werden. $0=<\vec s-\vec s_0,a\cdot\vec n>$ mit $a\neq0$ heisst __Normalengleichung (NG).__
2. $0=<\vec s-\vec s_0,\pm\hat n>$ heisst __Hesse-Normalform (HNF).__
3. Durch Einsetzen eines beliebigen Punktes $\vec P$ in die HNF erhält man den vorzeichenbehafteten Abstand des Punktes von der Ebene.

$\to$ Serie 2, Nr. 10-20

## 2.3 Flächen und Volumen
---
### 2.3.1 Flächen in 2D
---
*Situation:* Fläche des Parallelogramms zwischen 2 Vektoren.
![Para](../assets/9d55374a.png=400)

**Satz:** Flächensatz in der Ebene.
Die Vektoren $\vec v \land \vec w$ spannen ein Parallelogramm auf mit der Fläche 
$\boxed{A=|v_1\cdot w_2-v_2\cdot w_1|}$

*Beweis:* Es gilt
$\underline{\underline{A}}=g\cdot h=|\vec v|\cdot |\vec w|\cdot\cos(\frac{\pi}{2}-\alpha)=<\vec v,\vec w>=\underline{\underline{|-v_2\cdot w_1+v_1\cdot w_2|}}\qquad\qquad\blacksquare$

### 2.3.2 Flächen in 3D
---
*Situation:*
![Para](../assets/9d55374a.png=400)

**Satz:** Flächensatz im Raum
Die Vektoren $\vec v \land \vec w\in \mathbb R^3$ spannen ein Parallelogramm auf mit der Fläche 
$\boxed{A=|\vec v \times\vec w|}$
*Beweis:* Es gilt
$\underline{\underline{A}}=g\cdot h=|\vec v|\cdot |\vec w|\cdot\sin(\alpha)=\underline{\underline{|\vec v\times\vec w|}}\qquad\qquad\blacksquare$

### 2.3.3 Volumen in 3D
---
*Situation:*
![Spat](../assets/1b305b74.png=400)

**Satz:** Volumensatz im Raum 
Die Vektoren $\vec u,\vec v \land \vec w\in \mathbb R^3$ spannen einen Spat auf mit Volumen
$\boxed{V=|<\vec u,\vec v\times\vec w>|}$
*Beweis:* Es gilt
$\underline{\underline{V}}=G\cdot h=||\vec v|\cdot |\vec w|\cdot|\vec u|\cdot\cos(\beta)|=\underline{\underline{|<\vec u,\vec v\times\vec w>|}}\qquad\qquad\blacksquare$

Bemerkungen:
1. Masseinheit: $[V]=[\vec u]\cdot[\vec v]\cdot[\vec  w]$
2. Wegen der Spat-Zyklus-Regel und des Betrages dürfen die Vektoren beliebig getauscht werden. $\implies 3!$ Möglichkeiten.