---
title: IEM_I_2
subtitle: Integrationsmethoden
date: 15.03.2019
tag: IEM_I
---
# 2. Integrationsmethoden
---
## 2.1 Substitution
---
Idee: Umkehrung der Ketten-Regel

**Satz:** Substitutions-Regeln
Sei: $F'(u)=f(u)$. Dann gilt für eine Funktion $u(x)$:
$\int f(u(x))\cdot u'(x)dx=\int f(u)du=F(u(x))+c$
$\intop_{x_0}^{x_E}f(u(x))\cdot u'(x)dx=\intop_{u(x_0)}^{u(x_E)}=f(u)du=F(u(x_E))-F(u(x_0))$ boxed

*Beweis:* Gemäss Ketten-Regel gilt 
$(F(u(x))+c)'=(F(u(x)))'+0=\underbrace{F'(u(x))}_{\text{äussere}}-\underbrace{u'(x)}_{\text{innere}}$
$=\underline{\underline{f(u(x))\cdot u'(x)}}\qquad\qquad\blacksquare$

Beispiele:
+ $\int x\cdot\cos(x^2)dx\qquad$

Substitution: $u(x):=x^2\implies u'(x)=2x$
Variante 1: __Strukturelles Ergänzen__
$\int x\cdot\cos(x^2)dx=\int\frac 1 2\cdot 2x\cdot\cos(x^2)dx=\frac 1 2\int2x\cdot\cos(x^2)dx$
$=\frac 1 2\int\cos(u)du=\frac 1 2\cdot \sin(u)+c$
$=\underline{\underline{\frac 1 2\cdot\sin(x^2)+c}}$

Variante 2: __Kallkulieren mit dem Diffrentialsymbolen__
$\frac{du}{dx}=u'(x)=2x\implies du=2x\cdot dx\iff dx=\frac{1}{2x}\cdot du$
$\implies\underline{\underline{\int\cdot\cos(x^2)dx}}=\int x\cdot\cos(u)\cdot\frac{1}{2x}\cdot du=\frac 1 2\int\cos(u)du$
$\frac 1 2\sin(u)+c=\underline{\underline{\frac 1 2\sin(x^2)+c}}$

+ $\int\tan(x)dx=\int \frac{\sin(x)}{\cos(x)}dx$
Substitution:$u(x)=\cos(x)\implies\frac{du}{dx}=u'(x)=-\sin(x)$
$\implies dx=-\frac{1}{sin(x)}du$
$\implies\underline{\underline{\int\tan(x)dx}}=\int\frac{\sin(x)}{u}(-\frac{1}{\sin(x)})du=-\int\frac 1 u du$
$=-\ln(|u|)+c=\underline{\underline{-\ln(|cos(x)|)+c}}$

+ $I=\intop_0^1\sqrt{1-x^2}dx=\intop_0^1\sqrt{1-u^2}du$
Substitution: $u(\varphi)=\sin(\varphi)\implies\frac{du}{d\varphi}=u'(\varphi=\cos(\varphi)\implies du=\cos(\varphi)\cdot d\varphi$
$=\intop_{0}^{\frac \pi 2}\sqrt{1-\sin^2(\varphi)}\cdot\cos(\varphi)d\varphi=\intop_{0}^{\frac \pi 2}\sqrt{\cos^2(\varphi)}\cdot\cos(\varphi)d\varphi$
weil $\cos(\varphi)\ge 0$ für $\varphi\in\{0,\frac \pi 2\}$

Bemerkungen:
1. $u(x)\equiv$ Substitution (Ersetzung) Variablenwechsel: x to u oder u to x
2. Häufigster Fehler: Grenzen werden nicht angepasst!
3. In 90% der Anwendungen ist $u(x)$ __linear__ d.h. $u(x)=m\cdot x+q\to$ Abkürzung: __Lineare Modifikation__

$\to$ Serie 2,Nr. 1-4, Zusatzserie 2

Anwendung: Formel für die kinetische Energie.

*Situation:* 
![Hirsch](../assets/hirsch.png=400)
Der Körper wird durch eine beliebige Kraft $F(s)$ beschleunigt.
Beschleunigungsarbeit:
$\underline{\underline{\Delta W}}=\intop_{s_0}^{s_E}F(s)ds=\int_{t_0}^{t_E} F(s(t))\cdot \dot s(t)dt$
$=\intop_{t_0}^{t_E}m\cdot a(t)\cdot v(t)dt=m\intop_{t_0}^{t_E}v(t)\cdot\dot v(t)dt$
$=m\intop_{v_0}^{v_E}v\:dv=m\frac 1 2\cdot v^2|_{v_0}^{v_E}=\underline{\underline{\frac m 2 \cdot v_E^2-\frac m 2\cdot v_0^2}}$

Alternative über ACR-Prozess:
Schritt 1: Es gilt
$\underline{\Delta W}\approx F\cdot\delta s\approx F\cdot v\cdot dt=m\cdot a\cdot v\cdot dt$
$\qquad\approx\underline{m\cdot v\cdot\delta v}$

Schitt 2: Global: Durch Integration erhält man:
$\underline{\underline{\Delta W}}=m\intop_{v_0}^{v_E}v\:dv=m\frac 1 2\cdot v^2|_{v_0}^{v_E}=\underline{\underline{\frac m 2 \cdot v_E^2-\frac m 2\cdot v_0^2}}$

## 2.2 Partielle Integration
---
Idee: Umkehrung der Produkt-Regel
**Satz:** Partielle-Integrationsregel-Regeln
$\boxed{\int \overset{\darr}{g(x)}\cdot \overset{\uarr}{h'(x)}dx=g(x)\cdot h(x)-\int g'(x)\cdot h(x)dx}$
$\boxed{\int_{x_0}^{x_E} g(x)\cdot h'(x)dx=[g(x)\cdot h(x)]|_{x_0}^{x_E}-\int_{x_0}^{x_E} g'(x)\cdot h(x)dx}$

Beispiele:
+ $\underline{\underline{\int x\cdot e^x dx}}=x\cdot e^x-\int 1\cdot e^xdx=x\cdot e^x-e^x+c=\underline{\underline{e^x\cdot(x-1)+c}}$

+ $\int\ln(x)dx=\int\overset{\darr}{\ln(x)}\cdot \overset{\uarr}{1}dx=\ln(x)\cdot x-\int\frac 1 x\cdot xdx=\ln(x)\cdot x-\int1dx=\ln(x)\cdot x-x+c=\underline{\underline{x\cdot(\ln(x)-1)+c}}$

+ $F(x)=\int\sin^2(x)=\int\overset{\darr}{\sin(x)}\cdot\overset{\uarr}{\sin(x)}dx=\sin(x)(-1)\cdot\cos(x)-\int\cos(x)(-1)\cdot\cos(x)dx=-\sin(x)\cos(x)+\int\cos^2(x)dx=-\sin(x)\cos(x)+\int(1-\sin^2(x))=-\sin(x)\cos(x)+\int 1dx -\int\sin^2(x)=F(x)=-\sin(x)\cdot\cos(x)+x+b-F(x)=2F(x)=x-\sin(x)\cos(x)+b\implies \underline{\underline{F(x)=\frac{x-\sin(x)\cdot\cos(x)}{2}+c}}$

Bemerkungen:
1. Partielle Integration hat eine fundamentale theoretische Bedeutung: $\to$ Taylor-Entwicklungen, FEM-Theorie für Complatuter-Simulationen, Variationsrechnung
2. Es ist üblich im ersten Integral die Rollen der Faktoren durch Pfeile zu kennzeichnen. $\overset{\uarr}{h'x}$
3. Beim Aufleiten $h'\to h$ darf eine beliebige Stammfunktion von h' verwendet werden. **Wichtig:** In beiden Termen muss die __gleiche__ Stammfunktion h eingestellt werden.
4. Der 1-er- Trick: $\int f(x)dx=\int\overset{\darr}{f(x)}\cdot \overset{\uarr}{1}dx=f(x)\cdot x-\int f'(x)\cdot xdx$ Funktioniert gut für: $f\in\{\ln,\arcsin,\arccos,...,\text{arcsinh},\text{arccosh}\}$

**Satz:** Es gilt:
$\boxed{\int|x|dx=\frac 1 2\cdot \text{sgn}(x)\cdot x^2+c}$

*Beweis:* Es gilt:
$F(x)=\int|x|dx=\int\overset{\darr}{x}\cdot\overset{\uarr}{\text{sgn}(x)}dx$
$=x\cdot|x|-\int 1\cdot|x|dx=\underline{x\cdot|x|-F(x)+c}$
$\implies2F(x)=x\cdot|x|+c$
$\implies\underline{\underline{F(x)}}=\frac{x\cdot|x|+2}{2}=\frac{x\cdot\text{sgn}(x)\cdot x+c}{2}=\underline{\underline{\frac 1 2\cdot x\cdot\text{sgn}(x)\cdot x+c}}\qquad\qquad\blacksquare$

![sgn](../assets/sgn.png=300)

Beispiele:
+ $\int_{-1}^{2}|x|dx=\frac12[\text{sgn}(x^2)]\Big|_{-1}^{2}=\frac12\cdot\underbrace{\text{sgn}(2)}_{1}\cdot 2^2-\frac12\cdot\underbrace{\text{sgn}(-1)}_{-1}\cdot(-1)^2=\frac42\cdot\frac12=\underline{\underline{\frac52}}$

+ $\int_{0}^{1}|x-4|dx\overset{\text{LM}}{=}\frac11\cdot\frac{\text{sgn}(x-4)}{2}\cdot(x-4)^2\Big|_0^1$

+ $\Big\int_2^5|3x-8|dx=\frac13\cdot\frac12\cdot\text{sgn}(3x-8)(3x-8)^2\Big|_2^5$