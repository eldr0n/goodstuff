#!/usr/bin/env python3

import time
import shutil
import md2html
import yaml


ts = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())

file = open('ELA_2_L_2.md', 'r')
meta = yaml.dump(file)
content = md2html.addTags(file)
file.close()

# creating new index.html
shutil.move('public/index.html', 'archive/'+ts+'.html')
shutil.copy('archive/'+ts+'.html', 'public/index.html')

article = open('templates/article.html', 'r')
ar = article.readlines()
article.close()
art = []
for i in ar:
    i = i.replace('{{TITLE}}', meta['title'])
    i = i.replace('{{SUB}}', meta['subtitle'])
    i = i.replace('{{DATE}}', meta['date'])
    i = i.replace('{{TAG}}', meta['tag'])
    art.append(i)
art.reverse()


art = list(art)

file = open('public/index.html', 'r')
new = file.readlines()
file.close()

for i in art:
    new.insert(23, i)


file = open('public/index.html', 'w')
for i in new:
    file.write(i)
file.close()

# creating actual post
post = open('templates/post.html', 'r')
new = post.readlines()
post.close()

content.reverse()
content = list(content)


for i in content:
    new.insert(26, i)

newpost = open('public/posts/'+meta['subtitle']+'.html', 'w')
for i in new:
    newpost.write(i)
newpost.close()


