---
title: ELA-2-L 3
subtitle: Matrixrechnung
date: 09.04.2019
tag: ELA_2_L
---
# 3 Matrixrechnung
---
## 3.1 Grundbegriffe
---
### 3.1.1 Definition
---
**Definition:** Seien $m,n\in\mathbb N^+$. Eine __reelle mxn-Matrix__ ist eine Zahlentabelle der Form

$\boxed{A=\begin{pmatrix}A_{11}&A_{12}&...&A_{1n}\\A_{21}&A_{22}&...&A_{2n}\\.\\.\\.\\A_{m1}&A_{m2}&...&A_{mn}\\\end{pmatrix}}$

Wobei $A_{ij}\in\mathbb R$ für $i\in\{1,...,m \}\land j\in\{1,...,m \}$

Beispiele:
+ $\begin{pmatrix}2&3&-1\\ \sqrt{8}&7&0\end{pmatrix}\to 2\times3\text{-Matrix}$

+ $\begin{pmatrix}1\\2\end{pmatrix}\to 2\times1\text{-Matrix}$

+ $\begin{pmatrix}4&7&8\end{pmatrix}\to 1\times3\text{-Matrix}$

+ $\begin{pmatrix}5&-7\\8&2\end{pmatrix}\to 2\times2\text{-Matrix}$

Bemerkungen:
1. Eine mxn-Matrix hat m Zeilen und n Spalten.
2. Die reellen Zahlen $A_{ij}$ heissen __Komponenten__ der Matrix.
3. Die Menge der reellen mxn-Matrizen heisst $\mathbb M(m,n,\mathbb R)\lor\mathbb R^{m\times n}$

### 3.1.2 Operationen
---
#### 3.1.2.1 Addition & Subtraktion 
---
**Definition:** Seien $m,n\in\mathbb N^+\land A,B\in\mathbb R^{m\times n}$, dann definiert man 
$\boxed{A+B:=\begin{pmatrix}A_{11}+B_{11}&...&A_{1n}+B_{1n}\\.\\.\\.\\A_{m1}+B_{m1}&...&A_{mn}+B_{mn}\end{pmatrix}}$

$\boxed{A-B:=\begin{pmatrix}A_{11}-B_{11}&...&A_{1n}-B_{1n}\\.\\.\\.\\A_{m1}-B_{m1}&...&A_{mn}-B_{mn}\end{pmatrix}}$

Bemerkungen:
1. Nur Matrizen mit gleicher Anzahl Spalten und Zeilen können addiert bzw. subtrahiert werden.
2. Für $A,B\in\mathbb R^{m\times n}\implies (A\pm B)\in\mathbb R^{m\times n}$

Beispiel: 
$\begin{pmatrix}1&2\\0&3\end{pmatrix}+\begin{pmatrix}1&-2\\5&6\end{pmatrix}=\begin{pmatrix}2&0\\5&9\end{pmatrix}$


#### 3.1.2.2 Multiplikation mit einem Skalar
---
**Definition:** Seien $m,n\in\mathbb N^+,\;A\in\mathbb R^{m\times n}\land a\in\mathbb R$, dann definiert man:

$\boxed{a\cdot A:=A\cdot a:=\begin{pmatrix}a\cdot A_{11}&...&a\cdot A_{1n}\\.\\.\\.\\a\cdot A_{m1}&...&a\cdot A_{mn}\end{pmatrix}}$

Bemerkungen:
1. $A\in\mathbb R^{m\times n}\implies(a\cdot A)\in\mathbb R^{m\times n}$
2. MAn definiert für $a\neq 0:\frac A a:=\frac 1 a\cdot A$

Beispiel:
$2\cdot\begin{pmatrix}1&5\\0&1\end{pmatrix}=\begin{pmatrix}2&10\\0&2\end{pmatrix}$

#### 3.1.2.3 Transposition 
---
**Definition:** Seien Seien $m,n\in\mathbb N^+\land A\in\mathbb R^{m\times n}$. Die __Transponierte__ von $A$ ist:
$\boxed{A^T:=\begin{pmatrix}A_{11}&...&A_{m1}\\.\\.\\.\\A_{1n}&...&A_{mn}\end{pmatrix}}$

Bemerkungen: 
1. Beim Transponieren werden Zeilen und Spalten getauscht.
2. $A\in\mathbb R^{m\times n}\iff A^T\in\mathbb R^{n\times m}$
3. Die Transposition ist eine __Involution__ d.h. es gilt: ${(A^T)}^T=A$

Beispiel:
$\begin{pmatrix}1&2\\3&4\\5&6\end{pmatrix}^T=\begin{pmatrix}1&3&5\\2&4&6\end{pmatrix}$

#### 3.1.2.4 Matrix-Multiplikation
---
**Definition:** Seien $l,m,n\in \mathbb N^+, A\in\mathbb R^{l\times m} \land B\in\mathbb R^{m\times n},$ das Matrix-Produkt ist definiert durch:
$\boxed{C=A\cdot B \;\text{mit }C_{ij}:=\sum_{s=1}^m A_{is}\cdot{B_{sj}}}$

Bemerkungen:
1. $A\cdot B$ ist genau dann definiert, wenn __A__ so viele __Spalten__ wie __B Zeilen__ hat.
2. Für $A\in\mathbb R^{l\times m}\land B\in\mathbb R^{m\times n}\implies(A\cdot B)\in\mathbb R^{l\times n}$
3. Die Berechnung von Matrix-Produkten kann sehr aufwändig sein.

Beispiel:
$\begin{pmatrix}1\to2\\3\to4\end{pmatrix}=\begin{pmatrix}\underset{\downarrow}{5}&\underset{\downarrow}{6}\\7&8\end{pmatrix}=\begin{pmatrix}1\cdot 5+2\cdot 7&1\cdot 6+2\cdot 8\\3\cdot 5+4\cdot 7&3\cdot 6+4\cdot 8\end{pmatrix}=\begin{pmatrix}19&22\\43&50\end{pmatrix}$

### 3.1.3 Spezielle Matrizen
---
#### 3.1.3.1 Nullmatrix
---
**Definition:** Man schreibt
$\boxed{0=\begin{pmatrix}0\\0\end{pmatrix}=(0\;0\;0)=\begin{pmatrix}0&0\\0&0\end{pmatrix}=\begin{pmatrix}0&0&0\\0&0&0\end{pmatrix}}$

Bemerkungen:
1. Jede Matrix deren Komponenten __alle__ $0$ sind wird als $0$ geschrieben.
2. In Formeln soll jeweils die passende Zahlen- bzw. Spaltenzahl gewählt werden.
3. Es gelten die Fundamental-Regeln $\\A+0=0+A=A\\A\cdot 0=0\cdot A=0\\\text{Für \underline{jede} Matrix} \;A$

#### 3.1.3.2 Einheitsmatrix
---
**Definition:** Man Schreibt
$\boxed{\bf 1:=(1)=\begin{pmatrix}1&0\\0&1\end{pmatrix}=\begin{pmatrix}1&0&0\\0&1&0\\0&0&1\end{pmatrix}}$

Bemerkungen:
1. In Formeln soll jeweils die passende Zahlen- bzw. Spaltenzahl gewählt werden.
2. Es gilt die Fundamental-Regel $\\A\cdot \bf 1=\bf 1\cdot A=A\;|\text{Für \underline{jede} Matrix} \;A$

#### 3.1.3.3 Adjektive 
---
**1** $A$ __symmetrische__

**Definition:** $\boxed{A^T=A}$

Beispiele:
+ $0$
+ $\bf 1$
+ $\begin{pmatrix}1&2\\2&5\end{pmatrix}$
+ $\begin{pmatrix}1&2&3\\2&4&5\\3&5&6\end{pmatrix}$

**2** $A$ __schiefsymmetrische__

**Definition:** $\boxed{A^T=-A}$

Beispiele:
+ $0$
+ $\begin{pmatrix}0&-1\\1&0\end{pmatrix}$
+ $\begin{pmatrix}0&1&2\\-1&0&3\\-2&-3&0\end{pmatrix}$

*Wichtig:* auf der Diagonalen müssen immer $0$-en stehen!

Anzahl freie Komponenten $:\equiv\frac{n(n-1)}{2}$

**3** $A$ __quadratrische__

**Definition:** $\boxed{A\in\Bbb R^{n\times n}}$
$\implies$ gleichviele Zeilen wie Spalten.

### 3.1.4 Inversion
---
*Idee:* Kehrwert einer quadratischen Matrix.
**Definition:** Seien $n\in\Bbb N^+\land A\in\Bbb R^{n\times n}$. $A$ heisst __invertierbar__, falls es ein $A^{-1}\in\Bbb R^{n\times n}$ gibt mit:
$\boxed{A^{-1}\cdot A=\bf 1}$

Bemerkungen:
1. Es gibt viele invertierbare und nicht invertierbare Matritzen. Den meisten sieht man dies nicht ohne witeres an.
2. Synonyme: __regulär__ $\iff$ invertierbar __singulär__$\iff$ nicht invertierbar
3. Die Matrix $A^{-1}$ mit $A^{-1}\cdot A=\Bbb 1$ __Inverse__ von $A$.
4. Falls die Inverse $A^{-1}$ existiert, dann gilt $A^{-1}\cdot A=A\cdot A^{-1}=\Bbb 1$
5. Inversion ist eine Involution, d.h. ${(A^{-1})}^{-1}=A$