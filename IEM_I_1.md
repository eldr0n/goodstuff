---
title: IEM_I_1
subtitle: Elementare Integration
date: 22.02.2019
tag: IEM_I
---
# 1. Elementare Integration
---
## 1.1 Lineare Modifikation
---
Beispiele:

+ $\int\cos(5x-3)dx=\frac 1 5\sin(5x-3)+c$
+ $\int(7x+8)^5 dx=\frac 1 7 \cdot \frac 1 6 (7x+8)^6+c$
+ $\int 3^{5x+2}dx=\frac 1 5\cdot \frac{1}{\ln(3)}\cdot3^{5x+2}+c$

**Satz:** Regel für lineare Modifikation
Sei $F'(x)=f(x)$ dann gilt:
$\boxed{\int f(m\cdot x+q)dx=\frac 1 m\cdot F(m\cdot x+q)+c}$

*Beweis:* Zusatzserie 1, Nr. 1a

$\int A\cdot\sin(\omega t+\varphi)dt=\frac{1}{\omega}\cdot A(-\cos(\omega t+\varphi))+c$


## 1.2 Archimedes-Cauchy-Riemann Approximationsprozess
---
### 1.2.1 Einleitung
---

+ Konzept: $\boxed{\text{Anwendung}}\xrightarrow{ACR-Prozess}\boxed{\text Integral}\xrightarrow{\text{Integrationsmethoden, Tabellen, Numerik}}\boxed{Wert}$
+ Erklärung für die Schreibweise $\int fdx$

### 1.2.2 Historie des Integrals 
---
![Historie](../assets/hist_int.png=300)

1. Wähle $N\in\mathbb N^+$
2. Definiere $\Delta x:\frac{x_E-x_0}{N};\quad x_K:=x_0+K\cdot\Delta x\qquad|K\in \{1;N\}$
3. Flächen der Rechteckstreifen berechnen.$\\ \boxed{\Delta A_K=\Delta x\cdot f(x_N)}$
4. Für die gesamte Fläche gilt:$\\ \boxed{A\approx \sum_{K=1}^N f(x_K)\cdot \Delta x}$
5. Grenzwert $N\to\infty$ <br><br> $A=\underbrace{\lim\limits_{N\to\infty}\sum_{K=1}^N}_{\int_{x_0}^{x_E}}\underbrace{f(x_K)}_{f(x)}\cdot\underbrace{\Delta x}_{dx}$

### 1.2.3 Modernes Vorgehen
---
![Vorgehen](../assets/mod_vor.png=300)

1. __Lokal:__ Die Fläche des Streifens ist: $\Delta A\approx\Delta x\cdot f(x)$
2. __Global:__ Die gesamte Fläche ist: $A=\int_{x_0}^{x_E}f(x)dx=F(x_E)-F(x_0=...)$

### 1.2.4 Anwendungen
---
$\diamond$ __Trägtheitsmoment eines Stabes__ $(\text{Masse}\:m, \text{Länge}\: l)$

![Trägtheitsmoment](../assets/mom_stab.png=300)

1. __Lokal:__ Für das kleine Stück gilt:$\\ \Delta J\approx x^2\cdot\Delta m\underbrace{=}_{\frac{\Delta m}{m} = \frac{\Delta x}{l}}x^2\cdot\frac m l \cdot \Delta x=\underbrace{\frac m l \cdot x^2}_{f(x)\cdot\Delta x}\cdot\Delta x$
2. __Global:__ Für den gesamten Stab gilt: $\\ J=\int_{\frac{-1}{2}}^{\frac 1 2}\frac m l\cdot x^2dx=\frac m l\int_{\frac{-1}{2}}^{\frac 1 2}x^2dx=2\cdot\frac m l\int_{0}^{\frac 1 2}(x^2)dx\\=2\cdot\frac m l\cdot\frac 1 3\cdot x^3|_{0}^{\frac{1}{2}}=2\cdot\frac m l\cdot\frac 1 3\cdot\frac{l^3}{8}=\underline{\underline{\frac{1}{12}\cdot m\cdot l^2}}$

$\diamond$ __Kraft auf eine Staumauer__

![Staumauer](../assets/staumauer.png=300)

Wasserdruck: $p(z)=\rho\cdot g\cdot z\qquad\rho=Wasserdichte\approx 1.00\cdot 10^3 kg/m^3$

1. __Lokal:__ Die Kraft auf den Streifen:$\\ \Delta F\equiv p(z)\cdot \Delta A\equiv p(z)\cdot b(z)\cdot z$
2. __Global:__ Die Gesamte Kraft auf die Staumauer ist:$\\F:=\int_{x_0}^{x_E}(p(z)\cdot b(z))dz=...$

$\diamond$ __Anwendung in der IEM-E__

+ elektrische Felder + Spannung von kontinuirlichen Ladungsverteilungen.
+ Gesetz von Biot-Savard $\Delta\vec B\equiv\vec f(s)\cdot\Delta s$
+ Energie in Kondensatoren & Spulen.

