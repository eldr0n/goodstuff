---
title: ELA-1-K 1
subtitle: Vektorgeometrie
date: 1. Sem
tag: ELA_1_K
---
# 1. Vektorgeometrie
---
## 1.1 Bogenmass
---
Wie misst man Winkel?
+ Gradmass $\to [0°, 360°[$
+ Neugrad $\to [0gon, 400gon[$
+ Bogenmass $\to [0, 2\pi[\to$ geometrisch definiert

**Definition:**

![Definition Winkel](../assets/angle.png=400)

Der Winkel $\alpha$ ist definiert durch das Verhältnis:
$\boxed{\alpha=\frac{b}{r}}$

Bemerkungen:
1. Masseinheit $\alpha=\frac{b}{r}=\frac{m}{m}=1\to$ keine Masseinheit! Die Pseudoeinheit rad ist veraltet und unnötig.
2. Bereich: Vollkreis $\frac{2\pi\cdot r}{r}=2\pi\to$ Allgemein: $[0, 2\pi[$
3. Umrechnung:
    0°|30°|45°|60°|90°|180°|360°
    -|-|-|-|-|-|-
    0|$\frac{\pi}{6}$|$\frac{\pi}{4}$|$\frac{\pi}{3}$|$\frac{\pi}{2}$|$\pi$|$2\pi$
4. Tricks:
   * Halbkreis 180° = $\pi$
    * Bogenmasswinkel als Vielfaches von $\pi$ ausdrücken.
      α ≈ 1.16 ≈ 0.37$\pi\to$ gut vorstellbar
5. Umrechnungsformeln:
     $\alpha=\frac{\pi}{180\degree}\cdot\alpha_G$|$\alpha_G=\frac{180}{\pi}\cdot\alpha$
    -|-

**Satz** es gilt:
$\boxed{b=\alpha\cdot r}$
$\boxed{A=\frac{1}2\cdot \alpha\cdot r^2}$

### 1.2 Euklid-Räume
---
#### 1.2.1 Punktmengen
---

**1D:**
$\mathbb{R}^1\coloneqq\mathbb{R}$

![](https://upload.wikimedia.org/wikipedia/commons/d/d7/Real_number_line.svg =300x)

**2D:**
$\mathbb{R}^2\coloneqq\mathbb{R}\times\mathbb{R}=\{(x;y)|x,y\in\mathbb{R}\}$

![](https://upload.wikimedia.org/wikipedia/commons/4/49/Coord_XY.svg =300x)

**3D:**
$\mathbb{R}^3\coloneqq\mathbb{R}\times\mathbb{R}\times\mathbb{R}=\{(x;y;z)|x,y,z\in\mathbb{R}\}$

![](https://upload.wikimedia.org/wikipedia/commons/8/83/Coord_planes_color.svg =300x)

**$n$D:**
$\mathbb{R}^n\coloneqq\mathbb{R}\times...\times=\{(x_1;...;x_n)|x_1;...;x_n\in\mathbb{R}\}$
$\to n$ koordinaten
$\to n\in\mathbb{N}^+$ 

#### 1.2.2 Vektoralgebra
---
Vektorschreibweise

$\vec{x}=\begin{pmatrix}x_1 \\... \\x_n\end{pmatrix}$

MATLAB:
```octave
x = [1;2;3]
```
Man kann sagen:
Vektor ≈ Punkt mit algebraischer Struktur.

**Operation 1:** Vektoraddition für $\vec{x},\vec{y}\in\mathbb{R^n}$
$\boxed{\vec{x}+\vec{y}=\begin{pmatrix}x_1 \\... \\x_n\end{pmatrix}+\begin{pmatrix}y_1 \\... \\y_n\end{pmatrix}=\begin{pmatrix}x_1+y_1 \\... \\x_n+y_n\end{pmatrix}}$

Beispiel:

$\begin{pmatrix}7 \\-2\end{pmatrix}+\begin{pmatrix}3 \\1\end{pmatrix}=\begin{pmatrix}7+3 \\-2+1\end{pmatrix}=\begin{pmatrix}10\\-1\end{pmatrix}$

Geometrische Interpretation:

![](/home/rico/Boostnote/notes/vadd.png =300x)

$\vec{u}+\vec{v}=\vec{w}$

**Operation 2:** Multiplikation mit einem Skalar für $\vec{v}\land a \in\mathbb{R}$
$\boxed{a\cdot\vec{v}=a\cdot\begin{pmatrix}v_1\\v_2\end{pmatrix}=\begin{pmatrix}a\cdot v_1\\a\cdot v_2\end{pmatrix}}$

Geometrische Interpretation:
$\vec{v}=\rightarrow$
$a\vec{v}=\longrightarrow$
Streckung um den Faktor a.

#### Gramm-Riemann Skalarprodukt
---
Definition:
Seien $n\in\mathbb{N^+}\land\vec{v},\vec{w}\in\mathbb{R^n}$. Die reelle Zahl $\langle\vec{v},\vec w\rangle$ heisst Skalarprodukt.

$\boxed{\langle\vec v,\vec w\rangle=\Biggl\langle\begin{pmatrix}v_1\\...\\v_n\end{pmatrix}\cdot\begin{pmatrix}w_1\\...\\w_n\end{pmatrix}\Bigg\rangle=v_1\cdot w_1+...+v_n\cdot w_n}$

MATLAB:
```octave
sp = dot(v,w)
```
Haupteigenschaften:
1. Linearität: $\langle\vec v+\vec w,\vec u\rangle=\langle\vec v,\vec u\rangle+\langle\vec w,\vec u\rangle$
    $\langle a\cdot\vec v,\vec w\rangle=a\cdot\langle\vec v,\vec w\rangle$
2. Symetrie: $\langle\vec v,\vec w\rangle=\langle\vec w,\vec v\rangle$
3. positive Definitheit $\langle\vec v,\vec w\rangle \ge 0$

Bemerkungen:
1. Das Skalarprodukt ist eine reelle Zahl!
2. Masseinheit $[\langle\vec v,\vec w\rangle]=[\vec v]\cdot[\vec w]$
3. Schreibweise:
    * Standard $\langle\vec v,\vec w\rangle$
    * Funktionsanalysis $(\vec v,\vec w)$
    * Quantenphysik $\langle\vec v|\vec w\rangle$
    * Veraltet (NTB, Papula) $\vec v\cdot\vec w$
4. Das Skalarprodukt legt die gesamte Geometrie in einem Raum fest: Längen, Winkel, Flächen, Volumen und Masse.

#### 1.2.4 Längen und Winkel
---
Definition:
Sei $n\in\mathbb{N^+}\land\vec v\in\mathbb R^n.$ Die *Länge* von $\vec v$ ist:
$\boxed{|\vec v|=\sqrt{\langle\vec v,\vec w\rangle}}$

MATLAB:
```octave
l = norm(v)
```
Bemerkungen: 
1. Wurzel ist für alle $\vec v \in\mathbb R^n$ berechnbar.
2. In 2D und 3D stimmt die Länge mit der klassischen Vorstellung überein.

**Satz:** Seien $n\in\mathbb N^+\land\vec v,\vec w\in\mathbb R$ mit Zwischenwinkel $\alpha=\measuredangle(\vec v,\vec w)$, dann gilt:
$\boxed{\langle\vec v,\vec w\rangle=|\vec v|\cdot|\vec w|\cdot\cos(\alpha)}$

Bemerkungen:
1. $\langle\vec v,\vec w\rangle=0\implies\vec v\bot\vec w$

#### 1.2.5 Einheitsvektoren
---
Definition:
Sei $n\in\mathbb N^+\land\vec v\in\mathbb R^n\backslash\{0\}$
Der Einheitsvektor oder Richtungsvektor von $\vec v$ ist:
$\hat v:=\frac{1}{|\vec v|}$

MATLAB:
```octave
ev = v./norm(v)
```
Bemerkungen:
1. Es folgt $|\vec v|=\sqrt{\langle\vec v,\vec v\rangle}=1$
2. Einheitsvektoren haben *keine* Masseinheit.
3. Jeder Vektor lässt sich zerlegen $\vec v=|\vec v|\cdot\hat v$ (ausser 0).

#### 1.2.6 Orthogonalprojektion
---
Situation:

![32bf5adb.png](:storage/e6f12780-e1f2-407f-bc0a-50d43bc1ffcd/32bf5adb.png =300x)

**Satz:** Es gilt:
$\boxed{\vec w_\parallel=\langle\vec w,\hat v\rangle\cdot\hat v}$
$\boxed{\vec w_\bot=\vec w - \vec w_\parallel}$

#### 1.2.7 Grassmann-Vektorprodukt
---
Definition:
Seien $\vec v,\vec w\in\mathbb{R^3}.$ Der Vektor $\vec v\times\vec w$ heisst Grassman-Vektorprodukt oder Kreuzprodukt.
> $\boxed{\vec v\times\vec w:=\begin{pmatrix}v_2 w_3-v_3 w_2\\v_3 w_1-v_1 w_3\\v_1 w_2-v_2 w_1\end{pmatrix}}$

MATLAB:
```octave
vp = cross(v,w)
```
Bemerkungen:
1. Masseinheit $[\vec v\times\vec w]=[\vec v]\cdot[\vec w]$
2. Das Grassman-Vektorprodukt ist nur in 3D definiert.
3. In 2D Symplektische Form:
    <br/>
    * $\begin{pmatrix}v_1\\v_2\\0\end{pmatrix}\times\begin{pmatrix}v_1\\v_2\\0\end{pmatrix}=\begin{pmatrix}0\\0\\v_1 w_2-v_2 w_1\end{pmatrix}$
    <br/>
    * Definition: $\Omega(\vec v,\vec w)$

4. Hauptanwendung: *Drehmoment*
    Das Drehmoment der Kraft $\vec F_k$ ist $\vec M_k:=\vec r_k\times\vec F_k$

**Satz:** Seien $\vec v,\vec w\in\mathbb{R^3}.$ Dann gilt:
$\boxed{(\vec v\times\vec w)\bot\vec v\land(\vec v\times\vec w)\bot\vec w}$
$\boxed{|\vec v\times\vec w|=|\vec v|\cdot|\vec w|\cdot\sin(\measuredangle(\vec v,\vec w))}$

Rechte-Hand-Regel
![](:storage/e6f12780-e1f2-407f-bc0a-50d43bc1ffcd/rhand.png =300x)

**Satz: Orthoparialität**
Situation:

![32bf5adb.png](:storage/e6f12780-e1f2-407f-bc0a-50d43bc1ffcd/32bf5adb.png =300x)

Seien $\vec v,\vec w\in\mathbb{R^3}$. (Achtung! Abbildung einfachheitshalber in 2D)
Es gilt:
$\boxed{\langle\vec v,\vec w\rangle=\langle\vec v,\vec w_\parallel\rangle}$
$\boxed{\vec v\times\vec w=\vec v\times\vec w_\bot}$

**Satz: Flächensatz**
Situation:

![9d55374a.png](:storage/e6f12780-e1f2-407f-bc0a-50d43bc1ffcd/9d55374a.png =300x)

Seien $\vec v,\vec w\in\mathbb{R^3}$. (Achtung! Abbildung einfachheitshalber in 2D)
Es gilt:
$\boxed{A=|\vec v\times\vec w|}$

MATLAB:
```octave
A = norm(cross(v,w))
```

**Satz: Spatzsatz bzw. Volumensatz**
Situation:

![1b305b74.png](:storage/e6f12780-e1f2-407f-bc0a-50d43bc1ffcd/1b305b74.png =300x)

Es gilt:
$\boxed{V=\langle\vec u,\vec v\times\vec w\rangle}$

MATLAB:
```octave
V = dot(u,cross(v,w))
```
