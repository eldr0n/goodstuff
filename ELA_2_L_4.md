---
title: ELA-2-L 4
subtitle: Lineare_Abbildungen
date: 23.05.19
tag: ELA_2_L
---
# 4 Lineare Abbildungen
---
## 4.1 Definition
---
**Definition:** Eine lineare Abbildung ist eine Funktion der Form: $f:\mbb{R}^m\to\mbb{R}^n$, so dass für alle $\vec v,\vec w \in \mbb R^m\land a,b\in\mbb R gilt:$

$\boxed{f(a\cdot\vec v+b\vec w)=a\cdotf(\vec v)+b\cdot f(\vec w)}$
