#!/usr/bin/env python3

meta = {}

def dump(file):

    file.seek(4)

    for i in range(4):
        line = file.readline()
        line = line.strip()
        part = line.partition(': ')
        meta[part[0]] = part[2]
         
    return meta    
